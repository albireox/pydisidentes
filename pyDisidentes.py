#!/usr/bin/env python
# encoding: utf-8
"""
pyDisidentes.py

Created by José Sánchez-Gallego on 28 Apr 2013.
Copyright (c) 2013. All rights reserved.

"""

from twython import Twython
from datetime import datetime
from time import sleep


def checkNewDisidents(api, idsDone=[]):

    disidentGroups = [u'frente judaico popular', u'frente popular judaico',
                      u'frente popular del pueblo judio', u'frente del pueblo judio',
                      u'frente popular del pueblo judaico', u'union del pueblo judio',
                      u'frente popular de judea']

    for disidentGroup in disidentGroups:

        statuses = api.search(q=disidentGroup, rpp=50)

        for ss in statuses['statuses']:

            id = ss['id']

            user = ss['user']['screen_name']

            created_chunks = ss['created_at'].split()
            created = ' '.join(created_chunks[0:4]) + ' ' + created_chunks[-1]
            created = datetime.strptime(created, '%a %b %d %H:%M:%S %Y')
            tNow = datetime.utcnow()
            tDiff = (tNow - created)
            tDiff = tDiff.days * 24 * 60 + tDiff.seconds / 60.

            retweeted = True if 'retweeted_status' in ss else False

            if retweeted is False and tDiff < 30. and id not in idsDone:
                try:
                    if disidentGroup == u'frente popular de judea':
                        api.updateStatus(status=u'@%s ¡Disidente! Ah, no, espera, que esos somos nosotros.' % user)
                    else:
                        api.updateStatus(status=u'@%s ¡Disidente!' % user)
                except:
                    pass
                idsDone.append(id)

    sleep(60)
    checkNewDisidents(api, idsDone=idsDone)


def main():

    api = Twython(app_key='',
                  app_secret='',
                  oauth_token='',
                  oauth_token_secret='')

    while True:
        try:
            checkNewDisidents(api, idsDone=[])
        except:
            sleep(300)
            main()

    return


if __name__ == '__main__':
    main()
